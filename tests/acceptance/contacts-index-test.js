import { test } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contacts index');

test('visiting user contacts route displays all contacts', function(assert) {
  server.createList('contact', 5);

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact').length, 5, 'All contacts display');
  });
});

test('user contacts list is sorted by name', function(assert) {
  server.create('contact', { name: 'John Armstrong' });
  server.create('contact', { name: 'John Doe' });
  server.create('contact', { name: 'Jane Doe' });


  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Jane Doe', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(1) .test-contact-name', 'John Armstrong', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(2) .test-contact-name', 'John Doe', 'Contact list is sorted alphabetically');
  });
});

test('displays info for a contact in each row', function(assert) {
  let contact = server.create('contact', { name: 'Ann Smith' });
  server.create('emailAddress', { label: 'secondary', contactId: contact.id });
  server.create('emailAddress', { label: 'primary', email: 'smith@example.com', contactId: contact.id });
  server.create('emailAddress', { label: 'primary', contactId: contact.id });
  server.create('emailAddress', { label: 'primary', contactId: contact.id });
  server.create('phoneNumber', { label: 'secondary', contactId: contact.id });
  server.create('phoneNumber', { label: 'primary', number: '+1 555-555-5252', contactId: contact.id });
  server.create('phoneNumber', { label: 'primary', contactId: contact.id });

  contact = server.create('contact', { name: 'Luke Skywalker' });
  server.create('emailAddress', { label: 'primary', email: 'luke.skywalker@example.com', contactId: contact.id });
  server.create('phoneNumber', { label: 'primary', number: '+1 222-333-444', contactId: contact.id });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Ann Smith', 'Contact row displays contact name');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', 'smith@example.com', 'Contact row displays contact primary email');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', '(+2)', 'Displays additional email count');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '+1 555-555-5252', 'Contact row displays primary phone');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '(+1)', 'Displays additional phone count');

    assert.contains('.test-contact:eq(1) .test-contact-name', 'Luke Skywalker', 'Contact row displays contact name');
    assert.equal(find('.test-contact:eq(1) .test-contact-primary-email').text().trim(), 'luke.skywalker@example.com', 'Contact row displays contact primary email without count');
    assert.equal(find('.test-contact:eq(1) .test-contact-primary-phone').text().trim(), '+1 222-333-444', 'Contact row displays contact primary phone without count');
  });
});

// Write the tests
test('name links to edit for a contact', function(assert) {
  let contact = server.create('contact', { name: 'Ann Smith' });
  visit('/contacts');
  click('.test-contact:eq(0) .test-contact-name a:contains("Ann Smith")');
  andThen(function() {
    assert.equal(currentURL(), `/contacts/${contact.id}/edit`);
    assert.equal(find('.test-contact-name').text().trim(), 'Ann Smith');
  });
});


test('primary email is a mailto: link', function(assert) {
  let contact = server.create('contact', { name: 'Ann Smith' });
  server.create('emailAddress', { label: 'primary', email: 'smith@example.com', contactId: contact.id });
  visit('/contacts');
  andThen(function() {
    assert.equal(find('.test-contact:eq(0) .test-contact-primary-email a').attr('href'), 'mailto:smith@example.com', 'primary email is a mailto: link');
  });
});

test('primary phone is a tel: link', function(assert) {
  let contact = server.create('contact', { name: 'Ann Smith' });
  server.create('phoneNumber', { label: 'primary', number: '+1 555-555-5252', contactId: contact.id });
  visit('/contacts');
  andThen(function() {
    assert.equal(find('.test-contact:eq(0) .test-contact-primary-phone a').attr('href'), 'tel:+1 555-555-5252', 'primary phone is a tel: link');
  });
});
