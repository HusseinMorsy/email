import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  label() { return faker.random.arrayElement(['primary', 'primary', 'primary', 'alternate', 'work']) },
  email() { return faker.internet.email(); }
});
