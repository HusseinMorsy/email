import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  label() { return faker.random.arrayElement(['primary', 'alternate', 'work']) },
  number() { return faker.phone.phoneNumber() }
});
